﻿//=====================================================================================================================
//
//  WhiteRonin copyright 2019
//
//  Date        2019-10-24 14:00:00
//  Author      Dolwen (Jérôme DEMYTTENAERE) 
//  Email       jerome.demyttenaere@gmail.com
//  Project     Star Shooter (STA) for Unity3D
//
//  All rights reserved
//
//=====================================================================================================================

using UnityEngine;

//=====================================================================================================================
namespace StarShooter
{
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    /// <summary>
    /// Enemy Ship Controller
    /// </summary>
    public class GameBoundary : MonoBehaviour
    {
        //-------------------------------------------------------------------------------------------------------------
        void OnTriggerExit(Collider sOther)
        {
            Debug.Log("OnTriggerExit " + sOther.tag);
            Destroy(sOther.gameObject);

            // If Enemy Ship go out of screen hit the Player Ship
            if (sOther.tag.Equals("EnemyShip"))
            {
                EnemyShip tShip = sOther.GetComponent<EnemyShip>() as EnemyShip;
                tShip.DoAction();
            }
        }
        //-------------------------------------------------------------------------------------------------------------
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
//=====================================================================================================================