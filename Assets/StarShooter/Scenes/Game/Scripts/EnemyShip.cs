﻿//=====================================================================================================================
//
//  WhiteRonin copyright 2019
//
//  Date        2019-10-24 14:00:00
//  Author      Dolwen (Jérôme DEMYTTENAERE) 
//  Email       jerome.demyttenaere@gmail.com
//  Project     Star Shooter (STA) for Unity3D
//
//  All rights reserved
//
//=====================================================================================================================

using UnityEngine;

//=====================================================================================================================
namespace StarShooter
{
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    /// <summary>
    /// Enemy Ship Controller
    /// </summary>
    public class EnemyShip : MonoBehaviour
    {
        //-------------------------------------------------------------------------------------------------------------
        public float SpeedMin = 0f;
        public float SpeedMax = 10f;
        public int Score = 10;
        public int WeaponDamage = 10;
        //=============================================================================================================
        // PUBLIC METHOD
        //-------------------------------------------------------------------------------------------------------------
        public void SetRandPosition()
        {
            float tY = Random.Range(-3, 11);
            float tZ = Random.Range(-14, 5);

            transform.position = new Vector3
            (
                transform.position.x,
                transform.position.y + tY,
                transform.position.z + tZ
            );
        }
        //-------------------------------------------------------------------------------------------------------------
        public void DoAction()
        {
            // Hit the player ship if the enemy ship reach the edge of the screen (boundary)
            GameController.UnityShareInstance().HitPlayerShip(WeaponDamage);
        }
        //=============================================================================================================
        // PRIVATE METHOD
        //-------------------------------------------------------------------------------------------------------------
        void Start()
        {
            // Set initial random speed
            float tSpeed = Random.Range(SpeedMin, SpeedMax);
            GetComponent<Rigidbody>().velocity = transform.forward * tSpeed;

            /*
            // Modify speed for the 3 axis with a fixed value for up & right
            // Simulate a diagonal movement
            Vector3 tForward = transform.forward * tSpeed;
            Vector3 tRight = transform.right * 0.5f;
            Vector3 tUp = transform.up * 1f;

            // Combine the three component vectors
            Vector3 tVelocity = tUp + tRight + tForward;

            // Set the new velocity
            GetComponent<Rigidbody>().velocity = tVelocity;
            */
        }
        //-------------------------------------------------------------------------------------------------------------
        void OnTriggerEnter(Collider sOther)
        {
            // Don't destroy the Game Boundary
            if (!sOther.tag.Equals("Boundary"))
            {
                // Destroy both the enemy ship & the bolt (weapon)
                Destroy(sOther.gameObject);
                Destroy(gameObject);

                // Add score to the player
                GameController.UnityShareInstance().AddScore(Score);
            }
        }
        //-------------------------------------------------------------------------------------------------------------
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
//=====================================================================================================================