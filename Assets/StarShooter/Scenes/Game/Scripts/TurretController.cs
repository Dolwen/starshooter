﻿//=====================================================================================================================
//
//  WhiteRonin copyright 2019
//
//  Date        2019-10-24 14:00:00
//  Author      Dolwen (Jérôme DEMYTTENAERE) 
//  Email       jerome.demyttenaere@gmail.com
//  Project     Star Shooter (STA) for Unity3D
//
//  All rights reserved
//
//=====================================================================================================================

using UnityEngine;

//=====================================================================================================================
namespace StarShooter
{
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    /// <summary>
    /// Turret Controller
    /// </summary>
    public class TurretController : MonoBehaviour
    {
        //-------------------------------------------------------------------------------------------------------------
        public GameObject BoltPrefab;
        public Transform BoltSpawn;
        public float FireRate;
        //-------------------------------------------------------------------------------------------------------------
        private float NextFire;
        private float Horizontal = 0f;
        private float Vertical = 0f;
        //-------------------------------------------------------------------------------------------------------------
        void Update ()
        {
            // Spawn Bolt if user hit Fire key
            if (Input.GetButton("Fire1") && Time.time > NextFire && !GameController.UnityShareInstance().IsGameOver)
            {
                NextFire = Time.time + FireRate;
                Instantiate(BoltPrefab, BoltSpawn.position, BoltSpawn.rotation);
            }
        }
        //-------------------------------------------------------------------------------------------------------------
        void FixedUpdate ()
        {
            if (!GameController.UnityShareInstance().IsGameOver)
            {
                float tHorizontal = Input.GetAxis("Horizontal");
                float tVertical = Input.GetAxis("Vertical");

                Horizontal += tHorizontal;
                Vertical += tVertical;

                // Lock Horizontal turret movement
                if (Horizontal > 55) Horizontal = 55;
                if (Horizontal < -55) Horizontal = -55;

                // Lock Vertical turret movement
                if (Vertical > 20) Vertical = 20;
                if (Vertical < -40) Vertical = -40;

                GetComponent<Rigidbody>().rotation = Quaternion.Euler(Vertical, Horizontal, 0.0f);
            }
        }
        //-------------------------------------------------------------------------------------------------------------
    }    
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
//=====================================================================================================================