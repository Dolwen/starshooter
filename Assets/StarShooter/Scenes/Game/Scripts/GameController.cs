﻿//=====================================================================================================================
//
//  WhiteRonin copyright 2019
//
//  Date        2019-10-24 14:00:00
//  Author      Dolwen (Jérôme DEMYTTENAERE) 
//  Email       jerome.demyttenaere@gmail.com
//  Project     Star Shooter (STA) for Unity3D
//
//  All rights reserved
//
//=====================================================================================================================

using System.Collections;
using UnityEngine;
using UnityEngine.UI;

//=====================================================================================================================
namespace StarShooter
{
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    /// <summary>
    /// Game Controller
    /// </summary>
    public class GameController : MonoBehaviour
    {
        //-------------------------------------------------------------------------------------------------------------
        [Header("Game HUD")]
        public Text TextShield;
        public Text TextHP;
        public Text TextScore;
        public Toggle ToggleCockpit;
        public GameObject ImageCockpit;
        [Header("Game Config")]
        public float GameLenght = 0f;
        public GameObject TextGameOver;
        public GameObject TextYouWin;
        public bool IsGameOver { get; private set; }
        [Header("Enemy Ship Config")]
        public GameObject PrefabEnemySpawn;
        public GameObject RackEnemySpawn;
        [Header("Other Config")]
        public GameObject Planet;
        //-------------------------------------------------------------------------------------------------------------
        private static GameController Instance = null;
        private int Score = 0;
        private int Shield = 0;
        private int HP = 0;
        private float SpawnWait = 1f;
        private float Timer = 0f;
        private bool IsShowPlanet = false;
        //=============================================================================================================
        // PUBLIC METHOD
        //-------------------------------------------------------------------------------------------------------------
        public static GameController UnityShareInstance()
        {
            return Instance;
        }
        //-------------------------------------------------------------------------------------------------------------
        public void ResetGame()
        {
            InitGame();
        }
        //-------------------------------------------------------------------------------------------------------------
        public void AddScore(int sValue)
        {
            Score += sValue;
            TextScore.text = "" + Score;
        }
        //-------------------------------------------------------------------------------------------------------------
        public void HitPlayerShip(int sValue)
        {
            // Only take Play Ship Hit if game is not over!
            if (!IsGameOver)
            {
                // Minus Player Ship HP if Shield is depleted
                if (Shield <= 0)
                {
                    if (HP > 0)
                    {
                        HP -= sValue;
                        TextHP.text = "" + HP;
                    }

                    if (HP <= 0)
                    {
                        // Destroy the Player Ship if HP is depleted (Game Over)
                        GameOver();
                    }
                }
                else
                {
                    Shield -= sValue;
                    TextShield.text = "" + Shield;
                }
            }
        }
        //-------------------------------------------------------------------------------------------------------------
        public void SetCockpit()
        {
            ImageCockpit.SetActive(ToggleCockpit.isOn);
        }
        //=============================================================================================================
        // PRIVATE METHOD
        //-------------------------------------------------------------------------------------------------------------
        void Awake()
        {
            // Check if there is already an instance
            if (Instance == null)
            {
                // If not, set it to this.
                Instance = this;
            }
        }
        //-------------------------------------------------------------------------------------------------------------
        void Start()
        {
            InitGame();
        }
        //-------------------------------------------------------------------------------------------------------------
        void Update()
        {
            if (!IsGameOver)
            {
                // Set GameOver if we exceed the timer (game lenght)
                Timer += Time.deltaTime;
                if (Timer >= GameLenght)
                {
                    YouWin();
                    Timer = 0f;
                }

                // Start show planet animation
                if (Timer >= GameLenght - 8f && !IsShowPlanet)
                {
                    IsShowPlanet = true;
                    AnimPlanet("Show");
                }
            }
        }
        //-------------------------------------------------------------------------------------------------------------
        void InitGame()
        {
            // Set initial game parameters
            Score = 0;
            Shield = 100;
            HP = 20;
            TextScore.text = "" + Score;
            TextShield.text = "" + Shield;
            TextHP.text = "" + HP;

            // Reset HUD End Game
            IsShowPlanet = false;
            AnimPlanet("Reset");
            IsGameOver = false;
            TextYouWin.SetActive(false);
            TextGameOver.SetActive(false);

            // Clear the Rack of all Enemy Ship
            foreach (Transform child in RackEnemySpawn.transform)
            {
                Destroy(child.gameObject);
            }

            // Start spawning Enemy Ship
            StartCoroutine(SpawnWaves());
        }
        //-------------------------------------------------------------------------------------------------------------
        void GameOver()
        {
            IsGameOver = true;
            TextGameOver.SetActive(true);
        }
        //-------------------------------------------------------------------------------------------------------------
        void YouWin()
        {
            IsGameOver = true;
            TextYouWin.SetActive(true);
        }
        //-------------------------------------------------------------------------------------------------------------
        void AnimPlanet(string sTrigger)
        {
            Animator tAnim = Planet.GetComponent<Animator>();
            tAnim.SetTrigger(sTrigger);
        }
        //-------------------------------------------------------------------------------------------------------------
        IEnumerator SpawnWaves()
        {
            yield return new WaitForSeconds(1f);
            while (true)
            {
                // Instantiate a new Enemy Ship & Randomly modify his position
                GameObject tPrefab = Instantiate(PrefabEnemySpawn, RackEnemySpawn.transform, false);
                EnemyShip k = tPrefab.GetComponent<EnemyShip>() as EnemyShip;
                k.SetRandPosition();

                yield return new WaitForSeconds(SpawnWait);

                if (IsGameOver)
                {
                    break;
                }
            }
        }
        //-------------------------------------------------------------------------------------------------------------
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
//=====================================================================================================================