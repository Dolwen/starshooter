# Star Shooter

## Règles du jeu :
- Les vaisseaux ennemis apparaissent sur la gauche de l'écran de façon continue.
- Chaque vaisseau qui sort de l'écran (coté droit) touche le vaisseau du joueur.
- Le joueur doit tenir 1m30 en détruisant le plus de vaisseaux possible.

### Durée du test :
- 7h30

### Manquant :
- Ajout de 2 armes sup. (missile, emp)
- HUD armes sup. (missile / emp)

## Réalisé hors des 7 heures (durée estimé 4h) :
- Ship cinematique (splashscreen)
- Lock turret (left / right - up / down)
- Update spawn enemy ship position
- Add music / fx (music & bolt shoot)
- Change asset for interface (button)
- Eng Game (show a planet for "You Win - Safe zone")

## Bug :
- Collision entre vaisseaux ennemis (non géré)
